package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author ramses trejo - 23046
 *
 */

public class PasswordValidatorTest {
	
	
	@Test 
	public void testHasValidCaseCharsRegular(  ) {
		boolean result = PasswordValidator.hasValidCaseChars( "aaAaaAaaa" );
		assertTrue( "Invalid case characters" , result );
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn( ) {
		boolean result = PasswordValidator.hasValidCaseChars( "aA" );
		assertTrue( "Invalid case characters" , result );		
	}		
	
	
	@Test
	public void testHasValidCaseCharsExceptionBlank( ) {
		boolean result = PasswordValidator.hasValidCaseChars( "" );
		assertFalse( "Invalid case characters" , result );		
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull( ) {
		boolean result = PasswordValidator.hasValidCaseChars( null );
		assertFalse( "Invalid case characters" , result );		
	}	
	
	@Test
	public void testHasValidCaseCharsExceptionNumbers( ) {
		boolean result = PasswordValidator.hasValidCaseChars( "898989" );
		assertFalse( "Invalid case characters" , result );		
	}		
	
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper( ) {
		boolean result = PasswordValidator.hasValidCaseChars( "AAAA" );
		assertFalse( "Invalid case characters" , result );		
	}		

	@Test
	public void testHasValidCaseCharsBoundaryOutLower( ) {
		boolean result = PasswordValidator.hasValidCaseChars( "aaaa" );
		assertFalse( "Invalid case characters" , result );		
	}	
		
/********							***/
	
	@Test 
	public void testIsValidLengthRegular( ) {
		boolean result = PasswordValidator.isValidLength( "123456789012" );
		assertTrue(  "Invalid length" , result );
	}
	
	@Test 
	public void testIsValidLengthBoundaryIn( ) {
		boolean result = PasswordValidator.isValidLength( "12345678" );
		assertTrue( "Invalid length"  , result );
	}
		
	
	@Test 
	public void testIsValidLengthException( ) {
		boolean result = PasswordValidator.isValidLength( "" );
		assertFalse( "Invalid length"  , result );
	}
	
	@Test 
	public void testIsValidLengthExceptionSpaces( ) {	
		boolean result = PasswordValidator.isValidLength( "    t  e  s  t       " );
		assertFalse( "Invalid length"  , result );
	}
	
	@Test 
	public void testIsValidLengthBoundaryOut( ) {
		boolean result = PasswordValidator.isValidLength( "1234567" );
		assertFalse( "Invalid length"  , result );
	}	
	
	@Test 
	public void testHasValidDigitCountRegular( ) {
		boolean result = PasswordValidator.hasValidDigitCount("ss4sd8s4ds");
		assertTrue( "Invalid number of digits" , result );
	}
	
	@Test 
	public void testHasValidDigitCountBoundaryIn( ) {
		boolean result = PasswordValidator.hasValidDigitCount("kdk5jhjd4k8fjjjhjj");
		assertTrue( "Invalid number of digits" , result );
	}	
	
	@Test 
	public void testHasValidDigitCountException( ) {
		boolean result = PasswordValidator.hasValidDigitCount("kdkdkdkfj");
		assertFalse( "Invalid number of digits" , result );
	}	
	
	@Test 
	public void testHasValidDigitCountBoundaryOut( ) {
		boolean result = PasswordValidator.hasValidDigitCount("kdkd4kdkfj");
		assertFalse( "Invalid number of digits" , result );
	}	
	

}
